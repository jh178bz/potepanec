class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @taxon = @product.taxons.first
    @relation_products = @product.related_products.limit(DISPLAYED_NUMBER)
  end
end
