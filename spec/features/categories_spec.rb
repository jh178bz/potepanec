require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  include ApplicationHelper
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:product_bag) { create(:product, name: 'Bag', taxons: [taxon2]) }
  let(:product_shirts) { create(:product, name: 'T-shirts', taxons: [taxon3]) }
  let!(:taxon) { create(:taxon) }
  let!(:taxon2) { create(:taxon, taxonomy: taxonomy_1, parent_id: taxonomy_1.root.id) }
  let!(:taxon3) { create(:taxon, taxonomy: taxonomy_1, parent_id: taxonomy_1.root.id) }
  let(:taxonomy_1) { create(:taxonomy, name: 'Categories') }
  let(:taxonomy_2) { create(:taxonomy, name: 'Brand') }

  before { visit potepan_category_path(taxon.id) }

  scenario "products are correctly classified and displayed when user visit category page" do
    within '.sideBar' do
      expect(page).to have_content taxonomy_1.name
      expect(page).to have_content taxonomy_2.name
      expect(page).to have_content taxon.products.count
      click_link taxon.name
      expect(page).to have_current_path(potepan_category_path(taxon.id))
      expect(page).to have_title full_title(taxon.name)
    end
  end

  scenario 'T-shirts is not displayed when click Bags category' do
    visit potepan_category_path(taxon2.id)
    expect(page).to have_content product_bag.name
    expect(page).to have_no_content product_shirts.name
  end

  scenario 'user click product to go to single_product page' do
    click_on product.name
    expect(page).to have_current_path(potepan_product_path(product.id))
    click_link '一覧ページへ戻る'
    expect(page).to have_current_path(potepan_category_path(taxon.id))
  end
end
