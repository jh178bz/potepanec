require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:product_related_1) { create(:product, taxons: [taxon]) }
  let!(:product_related_2) { create(:product, taxons: [taxon]) }
  let!(:product_related_3) { create(:product, taxons: [taxon]) }
  let!(:product_related_4) { create(:product, taxons: [taxon]) }
  let!(:product_related_5) { create(:product, taxons: [taxon]) }
  let!(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let(:taxonomy) { create(:taxonomy) }

  before { visit potepan_product_path(product.id) }

  scenario "displayed correct content when user visit product page" do
    within '.media-body' do
      expect(page).to have_content product.name
      expect(page).to have_content product.price
      expect(page).to have_content product.description
    end

    within '.navbar' do
      click_on 'HOME'
      expect(page).to have_current_path('/potepan')
    end

    within '.lightSection' do
      visit potepan_product_path(product.id)
      expect(page).to have_content product.name
      click_on 'HOME'
      expect(page).to have_current_path('/potepan')
    end
  end

  feature "unrelation_product" do
    let!(:product_unrelated) { create(:product, taxons: [taxon2]) }
    let!(:taxon2) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }

    scenario "unrelated product is not displayed" do
      expect(page).to have_no_content product_unrelated.name
    end
  end

  feature "relation_products" do
    scenario "products are correctly displayed" do
      within '.productsContent' do
        expect(page).to have_content product_related_1.name
        expect(page).to have_no_content product.name
      end
    end

    scenario "product_related is four displayed" do
      within '.productsContent' do
        expect(page).to have_content product_related_1.name
        expect(page).to have_content product_related_2.name
        expect(page).to have_content product_related_3.name
        expect(page).to have_content product_related_4.name
      end
    end

    scenario "5th product is undisplayed" do
      within '.productsContent' do
        expect(page).to have_no_content product_related_5.name
      end
    end

    scenario "product detail is displayed and correctly link" do
      within '.productsContent' do
        expect(page).to have_content product_related_1.name
        expect(page).to have_content product_related_1.display_price
        click_on product_related_1.name
        expect(page).to have_current_path(potepan_product_path(product_related_1.id))
      end
    end
  end
end
