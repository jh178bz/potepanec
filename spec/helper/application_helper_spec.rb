require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe '#full_title' do
    it "is base_title only if title not provided" do
      expect(full_title('')).to eq 'BIGBAG Store'
      expect(full_title(nil)).to eq 'BIGBAG Store'
    end
    it "is full_title if title provided" do
      expect(full_title('Single products')).to eq 'Single products - BIGBAG Store'
    end
  end
end
