require 'rails_helper'

RSpec.describe "Categories", type: :request do
  describe "GET show" do
    let!(:taxon) { create(:taxon) }

    before { get potepan_category_path(taxon.id) }

    it 'responds succesfully' do
      expect(response).to have_http_status(200)
    end
  end
end
