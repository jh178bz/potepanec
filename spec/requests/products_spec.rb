require 'rails_helper'

RSpec.describe "Products", type: :request do
  describe "GET show" do
    let!(:product) { create(:product) }

    before { get potepan_product_path(product.id) }

    it 'responds succesfully' do
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end
  end
end
